export default [
    'pronoun_subject',
    'pronoun_stressed',
    'pronoun_object_direct',
    'pronoun_object_indirect',
    'possessive_determiner',
    'possessive_pronoun',
    'reflexive',
    'complement',
];
